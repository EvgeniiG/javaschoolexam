package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public static int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null) || inputNumbers.size() == 0) {
            throw new CannotBuildPyramidException();
        }
        int n = findRows(inputNumbers.size());
        if (n != 0) {
            int[][] pyramid = new int[n][2 * n - 1];
            Collections.sort(inputNumbers);
            int zero = 0;
            for (int i = 0; i < n; i++) {
                int k = n - i - 1;
                for (int j = 0; j <= i; j++) {
                    pyramid[i][k] = inputNumbers.get(zero);
                    zero += 1;
                    k += 2;
                }
            }
            return pyramid;
        } else {
            throw new CannotBuildPyramidException();
        }
    }

    /**
     * This method is used to find rows for our future 2D array.
     *
     * @param size This is the size of the given input list of integer values
     * @return int This returns number of rows
     */
    private static int findRows(int size) {
        for (int i = 1; i <= size; i++) {
            if (size == i * (i + 1) / 2) {
                return i;
            }
        }
        return 0;
    }
}
