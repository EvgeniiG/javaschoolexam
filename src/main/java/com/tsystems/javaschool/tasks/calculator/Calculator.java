package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            Stack<Double> operands = new Stack<>();
            Stack<Character> operators = new Stack<>();
            statement = statement.replaceAll("/s", "");
            char c;
            for (int i = 0; i < statement.length(); i++) {
                c = statement.charAt(i);
                if (c == '(')
                    operators.add('(');
                else if (c == ')') {
                    while (operators.lastElement() != '(')
                        evaluateRPN(operands, operators.pop());
                    operators.pop();
                } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                    while (!operators.isEmpty() && getPriority(operators.peek()) >= getPriority(c))
                        evaluateRPN(operands, operators.pop());
                    operators.add(c);
                } else {
                    StringBuilder operand = new StringBuilder();
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        operand.append(statement.charAt(i++));
                    }
                    i--;
                    operands.add(Double.parseDouble(operand.toString()));
                }
            }
            while (!operators.isEmpty())
                if (operands.get(1) == 0 && operators.pop().equals('/')) {
                    return null;
                } else {
                    evaluateRPN(operands, operators.pop());
                }
            return new DecimalFormat("##.####").format(operands.get(0));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * This method returns the priority of the operation
     *
     * @param operation This is the operation like "+", "-", "*", or "/".
     * @return int This returns the priority of the operation
     */
    static int getPriority(char operation) {
        switch (operation) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return 0;
        }
    }

    /**
     * This method Evaluate the value of an arithmetic expression in Reverse Polish Notation
     * depending on the given operation, adds, subtracts, multiplies or divides two numbers
     *
     * @param operands  This is the Stack of operands
     * @param operation This is the operation like "+", "-", "*", or "/".
     */
    private void evaluateRPN(Stack<Double> operands, char operation) {
        double a = operands.pop();
        double b = operands.pop();
        switch (operation) {
            case '+':
                operands.push(b + a);
                break;
            case '-':
                operands.push(b - a);
                break;
            case '*':
                operands.push(b * a);
                break;
            case '/':
                operands.push(b / a);
                break;
        }
    }
}
